CREATE TABLE "role"(
    "id" UUID PRIMARY KEY,
    "role" VARCHAR
);
CREATE TABLE "branch"(
  "id" UUID PRIMARY KEY,
  "name" VARCHAR,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP

);

CREATE TABLE "super_admin"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR,
    "password" VARCHAR,
    "login" VARCHAR UNIQUE,
    "role_id" UUID REFERENCES "role"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "manager"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR NOT NULL,
    "phone" VARCHAR NOT NULL,
    "saley" NUMERIC,
    "password" VARCHAR NOT NULL,
    "login" VARCHAR UNIQUE,
    "branch_id" UUID REFERENCES "branch"("id"),
    "role_id" UUID REFERENCES "role"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "teacher"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR ,
    "phone" VARCHAR UNIQUE,
    "password" VARCHAR NOT NULL,
    "login" VARCHAR UNIQUE,
    "salary" NUMERIC,
    "ielts_score" VARCHAR,
    "branch_id" UUID REFERENCES "branch"("id"),
    "role_id" UUID REFERENCES "role"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE "support_teacher"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR,
    "phone" VARCHAR UNIQUE,
    "password" VARCHAR NOT NULL,
    "login" VARCHAR UNIQUE,
    "salary" NUMERIC,
    "ielts_score" VARCHAR,
    "branch_id" UUID REFERENCES "branch"("id"),
    "role_id" UUID REFERENCES "role"("id"), 
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



CREATE TABLE "administrator"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR,
    "phone" VARCHAR UNIQUE,
    "password" VARCHAR NOT NULL,
    "login" VARCHAR UNIQUE,
    "salary" NUMERIC,
    "ielts_score" VARCHAR,
    "branch_id" UUID REFERENCES "branch"("id"),
    "role_id" UUID REFERENCES "role"("id"), 
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



CREATE TABLE "student"(
    "id" UUID PRIMARY KEY,
    "full_name" VARCHAR,
    "phone" VARCHAR UNIQUE,
    "password" VARCHAR NOT NULL,
    "login" VARCHAR UNIQUE,
    "salary" NUMERIC,
    "group_id" UUID REFERENCES "group"("id"),
    "branch_id" UUID REFERENCES "branch"("id"),
    "role_id" UUID REFERENCES "role"("id"), 
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE "group"(
    "id" UUID PRIMARY KEY,
    "uniqueID" VARCHAR NOT NULL UNIQUE,
    "branch_id" UUID REFERENCES "branch"("id"),
    "type" VARCHAR NOT NULL,
    "teacher_id" UUID REFERENCES "teacher"("id"),
    "support_teacher_id" UUID REFERENCES "support_teacher"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP

);


CREATE TABLE "event"(
    "id" UUID PRIMARY KEY,
    "topic" VARCHAR NOT NULL,
    "date" DATE NOT NULL,
    "start_time" TIME NOT NULL,
    "branch_id" UUID REFERENCES "branch"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "assign_student"(
    "id" UUID PRIMARY KEY,
    "event_id" UUID REFERENCES "event"("id"),
    "student_id" UUID REFERENCES "student"("id") UNIQUE,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "journal"(
    "id" UUID PRIMARY KEY,
    "group_id" UUID REFERENCES "group"("id"),
    "from" VARCHAR,
    "to" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE "schedule"(
    "id" UUID PRIMARY KEY,
    "journal_id" UUID REFERENCES "journal"("id"),
    "start_time" TIMESTAMP,
    "end_time" TIMESTAMP,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP

);

CREATE TABLE "lesson"(
    "id" UUID PRIMARY KEY,
    "schedule_id" UUID REFERENCES "schedule"("id") UNIQUE,
    "lesson" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "task"(
    "id" UUID PRIMARY KEY,
    "lesson_id" UUID REFERENCES "lesson"("id"),
    "label" VARCHAR,
    "deadline" TIMESTAMP,
    "score" NUMERIC,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "payment"(
    "id" UUID PRIMARY KEY,
    "student_id" UUID REFERENCES "student"("id"),
    "branch_id" UUID REFERENCES "branch"("id"), 
    "paid_sum" NUMERIC,
    "total_sum" NUMERIC,
    "course_count" INTEGER,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE "score"(
    "id" UUID PRIMARY KEY,
    "task_id" UUID REFERENCES "task"("id"),
    "student_id" UUID REFERENCES "student"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP

);







