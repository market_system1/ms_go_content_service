package grpc

import (
	"market_system/ms_go_content_service/config"
	"market_system/ms_go_content_service/genproto/crm_service"
	"market_system/ms_go_content_service/grpc/client"
	"market_system/ms_go_content_service/grpc/service"
	"market_system/ms_go_content_service/pkg/logger"
	"market_system/ms_go_content_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	crm_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	crm_service.RegisterSuperAdminServiceServer(grpcServer, service.NewSuperAdminService(cfg, log,strg,srvc))
	crm_service.RegisterManagerServiceServer(grpcServer, service.NewManagerService(cfg, log,strg,srvc))


	reflection.Register(grpcServer)
	return
}
