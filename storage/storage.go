package storage

import (
	"context"
	"market_system/ms_go_content_service/genproto/crm_service"
)

type StorageI interface {
	CloseDB()
	SuperAdmin() SuperAdminRepoI
	Branch() BranchRepoI
	Manager() ManagerRepoI
}

type SuperAdminRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateSuperAdmin) (resp *crm_service.SuperAdminPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.SuperAdminPrimaryKey) (resp *crm_service.SuperAdmin, err error)
	GetAll(ctx context.Context, req *crm_service.GetListSuperAdminRequest) (resp *crm_service.GetListSuperAdminResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateSuperAdmin) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.SuperAdminPrimaryKey) error
}

type BranchRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateBranch) (resp *crm_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.BranchPrimaryKey) (resp *crm_service.Branch, err error)
	GetAll(ctx context.Context, req *crm_service.GetListBranchRequest) (resp *crm_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateBranch) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.BranchPrimaryKey) error
}

type ManagerRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateManager) (resp *crm_service.ManagerPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.ManagerPrimaryKey) (resp *crm_service.Manager, err error)
	GetAll(ctx context.Context, req *crm_service.GetListManagerRequest) (resp *crm_service.GetListManagerResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateManager) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.ManagerPrimaryKey) error
}
